wget https://wordpress.org/latest.zip
mkdir -p /var/www/html
cp latest.zip /var/www/html/
cd /var/www/html/
unzip latest.zip
cp /wp-config.php wordpress/
sh env_init.sh
/usr/sbin/php-fpm7.3 --nodaemonize
