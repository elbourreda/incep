apt-get update
apt-get install mariadb-server -y
cp /50-server.cnf /etc/mysql/mariadb.conf.d/
service mysql start

mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO '$MSQL_USER'@'localhost' IDENTIFIED BY '$MSQL_USER_PASS';";
mysql -u root -e "GRANT ALL ON *.* to '$MSQL_USER'@'172.13.37.2' IDENTIFIED BY '$MSQL_USER_PASS';";
mysql -u root -e "ALTER USER 'root'@'localhost' IDENTIFIED BY '$MSQL_ROOT_PASS';";
mysql --password=$MSQL_ROOT_PASS --user=root -e "CREATE DATABASE wordpress;";
mysql --password=$MSQL_ROOT_PASS --user=root -e  "FLUSH PRIVILEGES;";

mysql --password=$MSQL_USER --user=$MSQL_USER wordpress < /wordpress.sql



